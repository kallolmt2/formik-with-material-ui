import React from 'react';
import {makeStyles } from '@material-ui/core/styles';
import {
  Container,
  Grid,
  Typography
} from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import TextField from './Components/FormsUI/TextField';
import Select from './Components/FormsUI/Select';
import DateTimePicker from './Components/FormsUI/DateTimePicker';
import countries from './data/countries.json';
import Checkbox from './Components/FormsUI/Checkbox';
import Button from './Components/FormsUI/Button';
import CustomImageInput from './Components/FormsUI/CustomFileUpload';

const useStyles = makeStyles((theme) => ({
  formWrapper: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(8)
  }
}))

const FILE_SIZE = 160 * 1024;
const SUPPORTED_FORMATS = [
  'image/jpg',
  'image/jpeg',
  'image/gif',
  'image/png'
]

const INITIAL_FORM_STATE = {
  firstName: '',
  lastName: '',
  email: '',
  phone: '',
  address1: '',
  address2: '',
  city: '',
  state: '',
  country: '',
  arrivalDate: '',
  departureDate: '',
  termsOfService: '',
  file: undefined
};

const FORM_VALIDATION = Yup.object().shape({
  firstName: Yup.string()
    .required('Required'),
  lastName: Yup.string()
    .required('Required'),
  email: Yup.string()
    .email('Invalid Email')
    .required('Required'),
  phone: Yup.number()
    .integer()
    .typeError('Please enter a valid phone number')
    .required('Required'),
  address1: Yup.string()
    .required('Required'),
  address2: Yup.string(),
  city: Yup.string()
    .required('Required'),
  state: Yup.string()
    .required('Required'),
  country: Yup.string()
    .required('Required'),
  arrivalDate: Yup.date()
    .required('Required'),
  departureDate: Yup.date()
    .required('Required'),
  termsOfService: Yup.boolean()
    .oneOf([true], 'The Terms and conditions must be accepted')
    .required('The Terms and conditions must be accepted'),
  file: Yup.mixed()
    .required('Required')
    .test('fileSize', 'File too large', value => value && value.size <= FILE_SIZE)
    .test('fileFormat', 'Unsupported Format', value => value && SUPPORTED_FORMATS.includes(value.type))
});

function App() {
  
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item xs={12}>
        <Container maxWidth='md'>
          <div className={classes.formWrapper}>
            <Formik 
             initialValues={{
              ...INITIAL_FORM_STATE
            }}
             validationSchema={FORM_VALIDATION}
             onSubmit={values => {
               console.log(values);
             }}
             render={({
               values,
               errors,
               touched,
               handleChange,
               handleBlur,
               setFieldValue
             }) => {
               return (
                <Form>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Typography>Your details</Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <Field
                      name='file'
                      component={CustomImageInput}
                      title='Upload Profile image'
                      setFieldValue={setFieldValue}
                      errorMessage={errors['file'] ? errors['file'] : undefined }
                      touched={touched['file']}
                      style={{display: 'flex'}}
                      onBlur={handleBlur}
                    />
                  </Grid>
                  
                  <Grid item xs={6}>
                    <TextField name='firstName' label='First Name' />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField name='lastName' label='Last Name' />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField name='email' label='Email' />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField name='phone' label='Phone' />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography>Address</Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <TextField name='address1' label='Address Line 1' />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField name='address2' label='Address Line 2' />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField name='city' label='City' />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField name='state' label='State' />
                  </Grid>
                  <Grid item xs={12}>
                    <Select name='country' label='Country' options={countries}/>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography>Booking Information</Typography>
                  </Grid>

                  <Grid item xs={6}>
                    <DateTimePicker name='arrivalDate' label='Arrival Date' />
                  </Grid>
                  <Grid item xs={6}>
                    <DateTimePicker name='departureDate' label='Departure Date' />
                  </Grid>

                  <Grid item xs={12}>
                    <Checkbox name='termsOfService' legend='Terms of Service' label='I agree' />
                  </Grid>

                  <Grid item xs={12}>
                    <Button>Submit Form</Button>
                  </Grid>

                </Grid>
              </Form>
                
               )
             }}
            >
              {/* <Form>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Typography>Your details</Typography>
                  </Grid>
                  
                  <Grid item xs={6}>
                    <TextField name='firstName' label='First Name' />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField name='lastName' label='Last Name' />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField name='email' label='Email' />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField name='phone' label='Phone' />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography>Address</Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <TextField name='address1' label='Address Line 1' />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField name='address2' label='Address Line 2' />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField name='city' label='City' />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField name='state' label='State' />
                  </Grid>
                  <Grid item xs={12}>
                    <Select name='country' label='Country' options={countries}/>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography>Booking Information</Typography>
                  </Grid>

                  <Grid item xs={6}>
                    <DateTimePicker name='arrivalDate' label='Arrival Date' />
                  </Grid>
                  <Grid item xs={6}>
                    <DateTimePicker name='departureDate' label='Departure Date' />
                  </Grid>

                  <Grid item xs={12}>
                    <Checkbox name='termsOfService' legend='Terms of Service' label='I agree' />
                  </Grid>

                  <Grid item xs={12}>
                    <Button>Submit Form</Button>
                  </Grid>

                </Grid>
              </Form> */}
            </Formik>
          </div>
        </Container>
      </Grid>
    </Grid>
  );
}

export default App;


 {/* <form>
                   <Field
                    name='file'
                    component={CustomImageInput}
                    title='Select a file'
                    setFieldValue={setFieldValue}
                    errorMessage={errors['file'] ? errors['file'] : undefined }
                    touched={touched['file']}
                    style={{display: 'flex'}}
                    onBlur={handleBlur}
                   />
                 </form> */}